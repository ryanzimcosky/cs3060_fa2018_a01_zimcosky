import sys
import threading
import time
import unittest

from BankAccount import BankAccount
from CheckingAccount import CheckingAccount
from SavingsAccount import SavingsAccount

class BankAccountTest(unittest.TestCase):

    def setUp(self):
        self.account = BankAccount()

    def test_newly_opened_account_has_zero_balance(self):
        self.assertEqual(self.account.getBalance(), 0)

    def test_can_deposit_money(self):
        self.account.deposit(100)
        self.assertEqual(self.account.getBalance(), 100)

    def test_can_deposit_money_sequentially(self):
        self.account.deposit(100)
        self.account.deposit(50)

        self.assertEqual(self.account.getBalance(), 150)

    def test_can_withdraw_money(self):
        self.account.deposit(100)
        self.account.withdraw(50)

        self.assertEqual(self.account.getBalance(), 50)

    def test_can_withdraw_money_sequentially(self):
        self.account.deposit(100)
        self.account.withdraw(20)
        self.account.withdraw(80)

        self.assertEqual(self.account.getBalance(), 0)


    def test_cannot_withdraw_more_than_deposited(self):
        self.account.deposit(25)

        with self.assertRaises(ValueError):
            self.account.withdraw(500)

    def test_cannot_withdraw_negative(self):
        self.account.deposit(100)

        with self.assertRaises(ValueError):
            self.account.withdraw(-50)

    def test_cannot_deposit_negative(self):
        with self.assertRaises(ValueError):
            self.account.deposit(-50)


    def test_transactions(self):

        localBankAccount = BankAccount(transactions=[])
        localBankAccount.deposit(100)
        localBankAccount.deposit(14.25)
        localBankAccount.withdraw(5.25)
        transactions = localBankAccount.getTransactions()
        
        self.assertEqual(len(transactions) == 3, True)

        for t in transactions:
            self.assertEqual("type" in t, True)
            self.assertEqual("amount" in t, True)

class CheckingAccountTest(BankAccountTest):

    def setUp(self):
        self.account = CheckingAccount()

    def test_inheritance(self):
        self.assertEqual(issubclass(CheckingAccount, BankAccount), True)

    def test_transactions(self):
        pass

class SavingsAccountTest(BankAccountTest):
    def setUp(self):
        self.account = SavingsAccount()

    def test_inheritance(self):
        self.assertEqual(issubclass(SavingsAccount, BankAccount), True)


    def test_can_deposit_money(self):
        with self.assertRaises(ValueError):
            self.account.deposit(100)

    def test_can_deposit_money_sequentially(self):

        with self.assertRaises(ValueError):
            self.account.deposit(100)

        with self.assertRaises(ValueError):
            self.account.deposit(50)


    def test_can_withdraw_money(self):
        
        with self.assertRaises(ValueError):
            self.account.withdraw(50)


    def test_can_withdraw_money_sequentially(self):

        with self.assertRaises(ValueError):
            self.account.deposit(100)
        
        with self.assertRaises(ValueError):
            self.account.withdraw(20)
        
        with self.assertRaises(ValueError):
            self.account.withdraw(80)

    def test_cannot_withdraw_more_than_deposited(self):
        with self.assertRaises(ValueError):
            self.account.deposit(25)

    def test_cannot_withdraw_negative(self):
        with self.assertRaises(ValueError):
            self.account.deposit(100)

    def test_cannot_deposit_negative(self):
        with self.assertRaises(ValueError):
            self.account.deposit(-50)

    def test_transfer(self):
        b = BankAccount()
        c = CheckingAccount()

        b.deposit(25); c.deposit(25)

        with self.assertRaises(TypeError):
            self.account.transferFrom(b, 25)
            self.account.transferTo(b, 25)

        self.account.balance += 25            
        origBalance = self.account.getBalance()
        self.account.transferTo(c, 25)
        self.assertEqual(origBalance - 25, self.account.getBalance())

        self.account.transferFrom(c, 25)
        self.assertEqual(origBalance, self.account.getBalance())

    def test_transactions(self):
        pass

if __name__ == '__main__':
    unittest.main()
