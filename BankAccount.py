#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 24 12:31:28 2018

@author: MisterZee
"""


class BankAccount:
    
    balance = 0.0
    interestRate = 0.0
    
   
    
    transactions = []
    
    def __init__(self, balance = 0.0, interestRate = 0.0, transactions = []):
        self.balance = balance
        self.interestRate = interestRate
        self.transactions = transactions
        
    def getBalance(self):
        return self.balance
    
    def getTransactions(self):
        return self.transactions
    
    def deposit(self, howMuch):
        if (howMuch > 0.0):
            self.balance = self.balance + howMuch
            print ("New Balance: ", self.balance)
            self.transactions.append(dict(type="deposit", amount=howMuch))
        else:
            print ("Invalid Amount, Balance Unchanged.")
            raise ValueError

    def withdraw(self, howMuch):
        if (self.balance - howMuch < 0.0):
            print ("Invalid Amount, Balance Unchanged")
            raise ValueError
        else:
            if (howMuch > 0.0):
                self.balance = self.balance - howMuch
                self.transactions.append(dict(type="withdrawl", amount=howMuch))
                print ("New Balance: ", self.balance)
            else:
                print ("Invalid Amount, Balance Unchanged.")
                raise ValueError
                
    def realizeInterest():
        pass
                
    
    