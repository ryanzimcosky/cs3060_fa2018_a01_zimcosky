#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 24 12:32:37 2018

@author: MisterZee
"""
from BankAccount import BankAccount

class CheckingAccount(BankAccount):
    
     def __init__(self, balance = 0.0, interestRate = 0.0, transactions = []):
        self.balance = balance
        self.interestRate = interestRate