#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 24 12:32:34 2018

@author: MisterZee
"""

from BankAccount import BankAccount

class SavingsAccount(BankAccount):
    
    def __init__(self, balance = 0.0, interestRate = 0.0, transactions = []):
        self.balance = balance
        self.interestRate = interestRate
        
    def withdraw(self, howMuch):
        print ("Cannot withdraw from Savings account.")
        raise ValueError
        
    def deposit(self, howMuch):
        print ("Cannot deposit from Savings account.")
        raise ValueError
        